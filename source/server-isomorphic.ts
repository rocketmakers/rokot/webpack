import * as fs from "fs";
import * as path from "path";
import * as webpack from "webpack";
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
import {excludeNodeModules, addPlugins, Rules, IWebpackSettings, createSourcePaths, ISourcePaths} from "./common";

export interface IIsomorphicServerSettings extends IWebpackSettings {
}

//const includePaths = [fs.realpathSync(path.resolve('node_modules/cosmos-ui')), fs.realpathSync(path.resolve("source"))]
export function getIsomorphicServerConfig(settings: IIsomorphicServerSettings): webpack.Configuration {
  const paths = createSourcePaths(settings)
  const node_modules = excludeNodeModules(paths.nodeModules, settings && settings.includeNodeModules);
  const tsConfigPath = path.join(paths.sourcePath, "tsconfig.json");

  const scssLoaders = [
    "isomorphic-style-loader",
    {
      loader: 'css-loader/locals',
      options: {
        importLoaders: 1
      }
    },
    {
      loader: 'postcss-loader',
      options: {
        plugins: function () {
          return [
            autoprefixer()
          ];
        }
      }
    },
    "resolve-url-loader",
    "sass-loader?indentedSyntax=false",
  ]

  const config: webpack.Configuration = {
    resolve: {
      extensions: [".ts", ".tsx", ".webpack.js", ".web.js", ".js"],
      modules: [paths.nodeModules]
    },
    target: "node",
    entry: settings.entry,
    output: {
      path: paths.outputPath,
      filename: "[name].js",
      libraryTarget: "commonjs"
    },
    devtool: "eval",
    externals: node_modules,
    module: {
      rules: [
        Rules.typescript(tsConfigPath),
        Rules.image(settings && settings.assetBundlerLimit),
        Rules.audio(settings && settings.assetBundlerLimit),
        Rules.video(settings && settings.assetBundlerLimit),
        Rules.font(),
        Rules.scss(scssLoaders)
      ]
    } as webpack.Module,
    plugins: []
  };

  addPlugins(config, settings)

  return config
}
