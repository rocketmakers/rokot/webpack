import * as fs from "fs";
import * as path from "path";
import * as webpack from "webpack";
import * as glob from "glob";
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const autoprefixer = require('autoprefixer');

import {excludeNodeModules, getBrowserScssLoaders, addPlugins, getWebFileExtensions, Rules, IWebpackSettings, IWebpackWebSettings, createSourcePaths, ISourcePaths} from "./common";

export interface IDevelopmentBrowserSettings extends IWebpackSettings, IWebpackWebSettings {
}

export function getDevelopmentBrowserConfig(settings: IDevelopmentBrowserSettings): webpack.Configuration {
  const paths = createSourcePaths(settings, "www")
  const tsConfigPath = path.join(paths.sourcePath, "tsconfig.json");
  const scssLoaders = getBrowserScssLoaders(paths.sourcePath, autoprefixer)

  const config: webpack.Configuration = {
    resolve: {
      extensions: getWebFileExtensions(),
      modules: [paths.nodeModules]
    },
    entry: settings.entry,
    devtool: "eval",
    output: {
      path: paths.outputPath,
      filename: "[name].js",
      publicPath: settings && settings.publicPath || ""
    },
    module: {
      rules: [
        Rules.font(),
        Rules.typescript(tsConfigPath),
        Rules.scss(scssLoaders),
        Rules.image(settings && settings.assetBundlerLimit),
        Rules.audio(settings && settings.assetBundlerLimit),
        Rules.video(settings && settings.assetBundlerLimit),
        Rules.masonry()
      ]
    },
    plugins: [
      new CaseSensitivePathsPlugin(),
      new webpack.LoaderOptionsPlugin({
        debug: true
      })
    ],
  };

  addPlugins(config, settings)

  return config
}
