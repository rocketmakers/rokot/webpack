import * as fs from "fs";
import * as path from "path";
import * as webpack from "webpack";
import * as glob from "glob";
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

import {excludeNodeModules, addPlugins, getBrowserScssLoaders, getWebFileExtensions, Rules, IWebpackSettings, IWebpackWebSettings, createSourcePaths, ISourcePaths} from "./common";

export interface IProductionBrowserSettings extends IWebpackSettings, IWebpackWebSettings {
}

export function getProductionBrowserConfig(settings: IProductionBrowserSettings): webpack.Configuration {
  const paths = createSourcePaths(settings, "www")
  const tsConfigPath = path.join(paths.sourcePath, "tsconfig.json");
  const scssLoaders = getBrowserScssLoaders(paths.sourcePath, autoprefixer, true)
  const extractTextPlugin = new ExtractTextPlugin({filename: "[name].css"});

  const config: webpack.Configuration = {
    resolve: {
      extensions: getWebFileExtensions(),
      modules: [paths.nodeModules]
    },
    entry: settings.entry,
    output: {
      path: paths.outputPath,
      filename: "[name].js",
      publicPath: settings && settings.publicPath || ""
    },
    module: {
      rules: [
        Rules.font(),
        Rules.typescript(tsConfigPath),
        Rules.scssExtract(extractTextPlugin, scssLoaders),
        Rules.image(settings && settings.assetBundlerLimit),
        Rules.audio(settings && settings.assetBundlerLimit),
        Rules.video(settings && settings.assetBundlerLimit),
        Rules.masonry()
      ]
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        output: {
          comments: false
        } as any,
        compress: {
          warnings: false
        }
      }),
      extractTextPlugin
    ],
  };

  addPlugins(config, settings)

  return config
}
