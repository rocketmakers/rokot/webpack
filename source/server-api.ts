import * as fs from "fs";
import * as path from "path";
import * as webpack from "webpack";
import * as glob from "glob";
const CopyWebpackPlugin = require('copy-webpack-plugin');
import {excludeNodeModules,addPlugins, Rules, ISourcePaths, IWebpackSettings, createSourcePaths} from "./common";

export interface IApiServerSettings extends IWebpackSettings {
}

export function getApiServerConfig(settings: IApiServerSettings): webpack.Configuration {
  const paths = createSourcePaths(settings)
  const node_modules = excludeNodeModules(paths.nodeModules, settings && settings.includeNodeModules);
  const tsConfigPath = path.join(paths.sourcePath, "tsconfig.json");
  const config: webpack.Configuration = {
    resolve: {
      extensions: [".ts", ".tsx", ".js"]
    },
    devtool: "eval",
    target: "node",
    entry: settings.entry,
    output: {
      path: paths.outputPath,
      filename: "[name].js",
      libraryTarget: "commonjs"
    },
    externals: node_modules,
    module: {
      rules: [
        Rules.typescript(tsConfigPath)
      ]
    },
    plugins: []
  }
  addPlugins(config, settings)

  return config
}
