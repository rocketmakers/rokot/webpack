import * as fs from "fs";
import * as webpack from "webpack";
import * as path from "path";
const CopyWebpackPlugin = require('copy-webpack-plugin');

export function createDefinePlugin(config: {[key:string]: any}){
  return new webpack.DefinePlugin(config)
}

export function excludeNodeModules(nodeModulesPath: string, includeNodeModules?: string[]) {
  return fs.readdirSync(nodeModulesPath).filter(function (x) { return x !== '.bin' && (!includeNodeModules || includeNodeModules.indexOf(x) === -1) });
}

export function getWebFileExtensions() {
  return [ ".ts", ".tsx", ".webpack.js", ".web.js", ".js", ".sass", ".scss" ]
}

export function addPlugins(config: webpack.Configuration, settings: IWebpackSettings) {
  if (settings && settings.plugins) {
    config.plugins.push(...settings.plugins)
  }

  if (settings && settings.copyFiles) {
    config.plugins.push(new CopyWebpackPlugin(settings.copyFiles))
  }
}

export function getBrowserScssLoaders(sourcePath: string, autoprefixer, isProduction = false) {
  const scssLoaders = [
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1
      }
    },
    {
      loader: 'postcss-loader',
      options: {
        plugins: function () {
          return [
            autoprefixer()
          ];
        }
      }
    },
    "resolve-url-loader",
    "sass-loader?indentedSyntax=false&includePaths[]=" + sourcePath
  ];
  if (!isProduction) {
    return [ "style-loader", ...scssLoaders ]
  }
  return scssLoaders
}

export interface ISourcePaths {
  entry: webpack.Entry
  /** The output path to write the compiled files to */
  outputPath: string
  /** The path to the typescript source root */
  sourcePath?: string
  /** The path to node_modules */
  nodeModules?: string
}

export interface IWebpackWebSettings {
  /** defaults to '/' */
  publicPath?: string
}

export interface IWebpackSettings extends ISourcePaths {
  /** Additional Plugins */
  plugins?: webpack.Plugin[]
  /** Add any npm module names that you want to be processed (ALL others are ignored)*/
  includeNodeModules?: string[]
  /** Optionally copy files after build */
  copyFiles?: { from: string, to?: string }[]
  /** Optionally set asset bundler limit for url loader - assets over this size will be exported as files rather than inline stylesheet data */
  assetBundlerLimit?: number
}

export function createSourcePaths(settings: IWebpackSettings, defaultRelativeOutputPath: string = "lib"): ISourcePaths {
  return {
    entry: settings.entry,
    nodeModules: settings.nodeModules || path.resolve("node_modules"),
    sourcePath: settings.sourcePath || path.resolve("source"),
    outputPath: settings.outputPath || path.resolve(defaultRelativeOutputPath),
  }
}

export class Rules {
  static scss(scssLoaders: any[]): webpack.UseRule {
    return {
      test: /\.scss$/,
      use: scssLoaders
    }
  }

  static typescript(tsConfigPath: string): webpack.UseRule {
    return {
      test: /\.ts(x)?$/,
      use: [ "ts-loader?tsconfig=" + tsConfigPath ]
    }
  }

  static font(): webpack.UseRule {
    return {
      test: /\.(ttf|eot|svg|woff2?)(\?[a-z0-9]+)?$/,
      use: [ 'file-loader' ]
    }
  }

  static image(limit?: number): webpack.UseRule {
    return {
      test: /\.(png|jpeg|jpg|gif)$/,
      use: [ `url-loader?limit=${limit || 15000}` ]
    }
  }

  static audio(limit?: number): webpack.UseRule {
    return {
      test: /\.mp3$/,
      use: [ `url-loader?limit=${limit || 15000}` ]
    }
  }

  static video(limit?: number): webpack.UseRule {
    return {
      test: /\.(mp4|m4v|webm)$/,
      use: [ `url-loader?limit=${limit || 15000}` ]
    }
  }

  static masonry(): webpack.UseRule {
    return {
      test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
      use: [ 'imports?define=>false&this=>window' ]
    }
  }

  static scssExtract(extractTextPlugin, scssLoaders: any[]): webpack.UseRule {
    return {
      test: /\.scss$/,
      use: extractTextPlugin.extract({
        fallback: "style-loader",
        use: scssLoaders
      })
    }
  }
}