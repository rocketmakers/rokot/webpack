
export interface IEnvironment {
  param: string;
  name: string;
  default: boolean;
}

export interface IEnvironmentSettings {
  environments?: IEnvironment[];
}

const defaults : IEnvironmentSettings = {
  environments: [
    { param: '-env-dev', name: 'dev', default: true },
    { param: '-env-test', name: 'test', default: false },
    { param: '-env-prod', name: 'prod', default: false },
  ]
}

export function getEnvironmentFromParam(settings? : IEnvironmentSettings) : string {

  settings = settings || defaults;

  if(!settings.environments.filter(e => e.default).length) {
    throw "Default environment missing";
  }

  let returnVal = settings.environments.filter(e => e.default)[0].name;

  if(!process || !process.argv || !process.argv.length) {
    return returnVal;
  }

  settings.environments.forEach(e => {
    if(process.argv.indexOf(e.param) !== -1) {
      returnVal = e.name;
    }
  });

  return returnVal;
}