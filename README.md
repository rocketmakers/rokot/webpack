Rokot - Webpack config generation
=====================================================================

`rokot-webpack` provides webpack configurations required to build typescript components based on the Rokot platform


## Example Usage
```
const path = require("path");
const webpackConfig = require('./lib');

const settings = {
  /** Required: The entry point */
  entry: {
    web: [path.resolve("source/index.tsx")]
  },

  /** Optional: Add any npm module names that you want to be processed by webpack (ALL others are ignored)*/
  includeNodeModules: ["front-end-module"],

  /** Optional: The output path (defaults to relative 'lib' for server, 'www' for browser) */
  outputPath: path.resolve("lib"),

  /** Optional: copy files after build (defaults undefined) */
  copyFiles: [{from: path.resolve("source/content/*"), to: path.resolve("lib/content/*")}],

  /** Optional: The path to the typescript source root (defaults as below) */
  sourcePath: path.resolve("source"),

  /** Optional: The path to node_modules (defaults as below)*/
  nodeModules: path.resolve("node_modules"),

  /** Optional: The public path for browser projects (defaults as below) */
  publicPath: ""

  /** Optional: The asset bundler limit for url loader - assets over this size will be exported as individual files rather than inline data (defaults as below) */
  assetBundlerLimit: 15000
}
```

### Browser - Production
```
module.exports = webpackConfig.getProductionBrowserConfig(settings)
```

### Browser - Development
```
module.exports = webpackConfig.getDevelopmentBrowserConfig(settings)
```

### Server - Api
```
module.exports = webpackConfig.getApiServerConfig(settings)
```

### Server - Isomorphic Web
```
module.exports = webpackConfig.getIsomorphicServerConfig(settings)
```
